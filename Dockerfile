FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app
COPY . /usr/src/app
RUN mvn clean package
EXPOSE 8080
EXPOSE $PORT
CMD java -Xmx510m -jar ./target/demo-0.0.1-SNAPSHOT.jar --server.port=$PORT
